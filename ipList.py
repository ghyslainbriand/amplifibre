import csv
import os
import sys
import subprocess
import re

class ipList:
    def getLocalIp():
            is_windows = sys.platform.startswith('win')
            if is_windows:
                return socket.gethostbyname(socket.gethostname())
            else :
                co = subprocess.Popen(['ifconfig'], stdout = subprocess.PIPE)
                ifconfig = str(co.stdout.read())
                #print(ifconfig)
                ip_regex = re.compile(r'[0-9]+(?:\.[0-9]+){3}')
                return (ip_regex.findall(ifconfig, re.MULTILINE)[0])    

    def data():
        try:
            cr = csv.reader(open("ListTrap.csv","r"))
            return cr
        except IOError:
            return None

    def add(newIP,newPort):
        present = False
        datas = ipList.data()
        if datas is not None:
            for row in  datas:
                if row[0] == newIP:
                    present = True
                    break
        if not present :
            cw = csv.writer(open("ListTrap.csv","a"))
            cw.writerow([newIP,newPort])
            return True
        else:
            return False

    def remove(iP):
        iPs = ipList.data()
        os.remove("ListTrap.csv")
        for row in iPs:
            cw = csv.writer(open("ListTrap.csv","a"))
            if row[0] != iP:
                cw.writerow(row)
            return True
        else:
            return False

## For test
if __name__ == '__main__':
    ipList.add("193.136.87.117","162")
    for row in ipList.data():
        print(row[1])
    ipList.remove("193.136.87.117")
