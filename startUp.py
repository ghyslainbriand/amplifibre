#!/usr/bin/python3

from ConfigAgentSNMP import *
import sendTraps
import threading
import time
import os




def StartCherryServ():
    print("")
    cherrypy.quickstart(ConfigSNMP(), config=ConfigEnv().conf) 
        
if __name__ == '__main__':
    ##os.system("sudo snmpd -c /etc/snmp/snmpd.conf")
    thCherry = threading.Thread(target = StartCherryServ)
    thCherry.start()
    st = sendTraps.sendTrap()
    thnotify = threading.Thread(target = st.notifyTrap()) 
    thnotify.start()
    thnotify.join()
    
        
