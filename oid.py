class oid:
    
    def __init__(self,complet = True):
        global __complet
        __complet = complet

    def base(self):
        return '1.3.6.1.4.1.43067'

    def general(self,index):
        reuslt =  '1.1.1.{}.0'.format(index)
        if __complet:
            return self.base() + '.' + reuslt
        else:
            return reuslt
    
    def pointCtrl(self,index):
        reuslt =  '1.1.2.{}.0'.format(index)
        if __complet:
            return self.base() + '.' +reuslt
        else:
            return reuslt

    def consigne(self,index):
        reuslt =  '1.1.3.{}.0'.format(index)
        if __complet:
            return self.base() + '.' + reuslt
        else:
            return reuslt
    
    def commande(self,index):
        reuslt =  '1.1.4.{}.0'.format(index)
        if __complet:
            return self.base() + '.' + reuslt
        else:
            return reuslt
    
    def trap(self,index):
        reuslt =  '1.1.5.{}.0'.format(index)
        if __complet:
            return self.base() + '.' +reuslt
        else:
            return reuslt

if __name__ == '__main__':
    oid = oid(False)
    print(oid.general(1))
    input()
