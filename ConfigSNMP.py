#!/usr/bin/python3

# Import CherryPy global namespace
import os
import cherrypy
import os.path
import socket
#from cherrypy.lib import static
from ipList import *

#os.chdir("/home/pi/") #modifier l'@ ip
os.chdir("E:\DEV\Python\CherryPyServeur\CherryPyServeur\CherryPyServeur")
def verifIP(TextIP1,TextIP2,TextIP3,TextIP4,TextPort):
    if TextIP1.isdigit() and TextIP2.isdigit()and TextIP3.isdigit() and TextIP4.isdigit() and TextPort.isdigit():
        if int(TextIP1) < 256 and int(TextIP2) < 256 and int(TextIP3) < 256 and int(TextIP4) < 256 and int(TextPort) < 999:
            return True
        else:
            return False
    else:
        return False

def verifUser():
        if cherrypy.session['username'] > '' and cherrypy.session['password'] == "2512":
            return True
        else:
            return False
    
conf = {
    '/':
    {
        'tools.staticdir.root':os.getcwd(),
        'tools.staticdir.debug':True,
        'tools.sessions.on': True,
        'log.screen': True
    },
        
    
        '/images': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir':  os.getcwd()+'/images'
        },
      
    
        'global':{
            'server.socket_host' : '192.168.87.123',
            'server.socket_port' : 80,
            'server.thread_pool' : 10
            }
        }

class ConfigSNMP:
    """ Sample request handler class. """

    


    def index(self):
       return open("Session.html","rt")      
    index.exposed = True


    def AdminSnmp(self,username = "" ,password =""):
#        file = open("PySnmp.html","rt")
     
        cherrypy.session['username'] = username
        cherrypy.session['password'] = password
        if verifUser() :
            return open("PySnmp.html","rt")
        else:
            return open("Session.html","rt")
    AdminSnmp.exposed = True
##        return '''
##            <form action="MakeIt" method="POST">
##            What is your name?
##            <input type="text" name="TextIP1" />
##            <input type="text" name="TextIP2" />
##            <input type="text" name="TextIP3" />
##            <input type="text" name="TextIP4" />
##            <input type="text" name="TextPort" />
##            <button name  = "Operation"  type = "submit" value = "add" > Ajoute</button>
##            <button name  = "Operation"  type = "submit" value = "suppr" > Supprime </button>
##            </form>
##            <a href='download'><button>This one</button></a>
##            '''
##        return """<img src="images/logo2.png" alt="Main Page" />"""

    index.exposed = True

    
        
    def MakeIt(self,TextIP1 = None,TextIP2 = None,TextIP3 = None,TextIP4 = None,TextPort = None,Operation = None):
        if verifUser() :
            if verifIP(TextIP1,TextIP2,TextIP3,TextIP4,TextPort):
                if Operation == "add":
                    if (ipList.add(TextIP1 + "."  + TextIP2 + "." + TextIP3 + "." + TextIP4 , TextPort)):
                        return  "Ajout de " + TextIP1 + "."  + TextIP2 + "." + TextIP3 + "." + TextIP4 + ":" + TextPort + '&nbsp;&nbsp;<a href="./">retour</a>.'
                    else:
                        return  "L'adresse " + TextIP1 + "."  + TextIP2 + "." + TextIP3 + "." + TextIP4 + ":" + TextPort + ' ne peux &ecirc;tre ajout&eacute;e&nbsp&nbsp<a href="./">retour</a>.'
                else:
                    if ipList.remove(TextIP1 + "."  + TextIP2 + "." + TextIP3 + "." + TextIP4):
                        return  "Suppression de " + TextIP1 + "."  + TextIP2 + "." + TextIP3 + "." + TextIP4 + '<a href="./">retour</a>.'
                    else:
                        return  "L'adresse " + TextIP1 + "."  + TextIP2 + "." + TextIP3 + "." + TextIP4  + ' ne peux &ecirc;tre supprim&eacute;e&nbsp&nbsp<a href="./">retour</a>.'
            else :
                return 'Veuillez saisir une adresse Ip Valide ! <a href="./">retour</a>.'       
        else:
            return open("Session.html","rt")
    MakeIt.exposed = True

   
    def download(self):
        if verifUser() :
            path = "/usr/share/mibs/VFI/VFISNMP-MIB"
            return static.serve_file(path, "application/x-download",
                                 "attachment", os.path.basename(path))
        else:
            return open("Session.html","rt")
    download.exposed = True

    def reboot(self):
        if verifUser() :  
            os.system("reboot")
            return open("Reboot.html")
        else:
            return open("Session.html","rt")
    reboot.exposed = True

def StartCherryServ():
    
    cherrypy.quickstart(ConfigSNMP(), config=conf)              
    

if __name__ == '__main__':
    print (os.getcwd())           
    StartCherryServ()
#else:
    # This branch is for the test suite; you can ignore it.
    #cherrypy.tree.mount(ConfigSNMP(),"/", config=conf)
