import oid
from pysnmp.entity.rfc3413.oneliner import cmdgen

class getTrapValues:
    def __init__(self, localIP):
        global  __cmdGen, __OldValue, __Traps, __localIP
        
        __cmdGen = cmdgen.CommandGenerator()
        __oid = oid.oid(True)
        __Traps = [__oid.trap(1), __oid.trap(3), __oid.trap(5), __oid.trap(7)]
        __OldValue = ["b'aucun'","0","0","0"]
        __localIP = localIP

    def read(self):
        result = {}
        __errorIndication, __errorStatus, __errorIndex, __varBinds = __cmdGen.getCmd(
            cmdgen.CommunityData('public'),
            cmdgen.UdpTransportTarget((__localIP, 161)),
            *__Traps
            )

        # Check for errors and print out results
        if __errorIndication:
            print(__errorIndication)
        else:
            if __errorStatus:
                print('%s at %s' % (
                    __errorStatus.prettyPrint(),
                    __errorIndex and __varBinds[int(__errorIndex)-1] or '?'
                    )
                )
            else:
                i=0
                for name, val in __varBinds:
                    newValue = str(val.prettyPrint())
##                    print('%s == %s' % (name.prettyPrint(), newValue))
                    if newValue != __OldValue[i]:
                        __OldValue[i] = newValue 
                        #result[__Traps[i]] = (str(name.prettyPrint()),newValue)
                        result[__Traps[i][:-2]] = newValue
                        
                    i = i + 1
        return result

if __name__ == '__main__':
    getTraps = getTrapValues('192.168.87.128')
    for i in range(10):
        Values = getTraps.read()
        for key in Values:
            print(Values[key])
    input()
