#!/usr/bin/python3

#
# Notification Originator
#
# Send SNMP INFORM notifications to multiple Managers using the
# following options:
#
# * SNMPv2C
# * send INFORM notification
# * to multiple Managers FromIPList
# * with TRAP ID 'coldStart' specified as an OID
# * include managed objects information:
#   1.3.6.1.2.1.1.1.0 = 'Example Notificator'
#
from pysnmp.entity.rfc3413.oneliner import ntforg
from pysnmp.proto.api import v2c
from ipList import *
from ConfigAgentSNMP import *
import getTrapValues
import threading
import time
import os

class sendTrap:
    def __init__(self):
        global  __ntfOrg, __getTrapValues

        __getTrapValues = getTrapValues.getTrapValues(ipList.getLocalIp())
        __ntfOrg = ntforg.NotificationOriginator()

    def __IpTrap(self, Ip):
        result = Ip.split('.')
        result[len(result)-1] = str(int(result[len(result)-1])+1)
        return ".".join(result)        
        
    def send(self):
        global  __ntfOrg
        Values = __getTrapValues.read()
        errorIndication = None
        for key in Values:
            for ip in ipList.data():
                
                # Build and submit notification message to dispatcher
                if (Values[key][0]).isnumeric():
                    print("sendNotification 1")
                    errorIndication = __ntfOrg.sendNotification(
                        ntforg.CommunityData('public'),
                        ntforg.UdpTransportTarget((ip[0], ip[1])),
                        'trap',
                        self.__IpTrap(key),
                        (key , v2c.Integer32( Values[key][0]) )
                    )

                else:
                    print("sendNotification 2")
                    errorIndication = __ntfOrg.sendNotification(
                        ntforg.CommunityData('public'),
                        ntforg.UdpTransportTarget((ip[0], ip[1])),
                        'trap',
                        self.__IpTrap(key),
                        ( key, v2c.OctetString( Values[key][0]) )
                    )   

        
        if errorIndication:
            print('Notification not sent: %s' % errorIndication)

    def notifyTrap(self):
        while True:
            print ('Notifystart')    
            self.send()
            time.sleep(15)


if __name__ == '__main__':
    st = sendTrap()
    thnotify = threading.Thread(target = st.notifyTrap()) 
    thnotify.start()
    thnotify.join()
        
