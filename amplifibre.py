#!/usr/bin/python3 -u

import re
import os
import glob
import subprocess
import sys
import threading
import time
import oid
import demandeReponse

import snmp_passpersist as snmp


__dr = demandeReponse.demandeReponse()
__oid = oid.oid(False)
__consignePuissancePompeAChange = __dr.GetPuissance_pompe_A_mW()
__consignePuissancePompeABruteChange = __dr.GetPuissance_pompe_A_brute()
__consignePuissancePompeBChange = __dr.GetPuissance_pompe_B_mW()
__consignePuissancePompeBBruteChange = __dr.GetPuissance_pompe_B_brute()
__commandeLaserA = __dr.GetLaserA()
__commandeLaserB = __dr.GetLaserB()

    
def update():
    global __consignePuissancePompeAChange
    global __consignePuissancePompeABruteChange
    global __consignePuissancePompeBChange
    global __consignePuissancePompeBBruteChange
    global __commandeLaserA
    global __commandeLaserB
    global __dr
    global __oid 
    global __oidC

    __dr.readReponse()
    
    
    #General
    pp.add_str(__oid.general(1), 'VFI-System') #Vendor 'VFI-System'
    pp.add_str(__oid.general(2), 'Amplifibre') #Module
    pp.add_str(__oid.general(3), '1.0') #HWVer
    pp.add_str(__oid.general(4), '09/12/2013') #HWRev
    pp.add_str(__oid.general(5), '1.0') #SWVer
    pp.add_str(__oid.general(6), '09/12/2013') #SWRev
    pp.add_str(__oid.general(7), '123A') #SerNuM
    pp.add_str(__oid.general(8), 'A') #CatNuM 'A'
    pp.add_str(__oid.general(9), '09/12/2013') #prodDate
    pp.add_str(__oid.general(10), 'Blaise Pascal Lannion') #Location desc
    pp.add_int(__oid.general(11), 1) #nombre d'ampli
    pp.add_str(__oid.general(12), 'Nom de l equipement') #nom equipement

    #Point de controle
    pp.add_str(__oid.pointCtrl(1), __dr.GetValue("ph1","dBm")) #psph1_dbm
    pp.add_int(__oid.pointCtrl(2), int(__dr.GetValue("ph1","brute"))) #psph1_Brute
    pp.add_str(__oid.pointCtrl(3), __dr.GetValue("ph2","dBm")) #psph2_dbm
    pp.add_int(__oid.pointCtrl(4), int(__dr.GetValue("ph2","brute"))) #psph2_Brute
    pp.add_str(__oid.pointCtrl(5), __dr.GetValue("ph3","dBm")) #psph3_dbm
    pp.add_int(__oid.pointCtrl(6), int(__dr.GetValue("ph3","brute"))) #psph3_Brute
    pp.add_str(__oid.pointCtrl(7), __dr.GetValue("ph4","dBm")) #psph4_dbm
    pp.add_int(__oid.pointCtrl(8), int(__dr.GetValue("ph4","brute"))) #psph4_Brute
    pp.add_str(__oid.pointCtrl(9), __dr.GetValue("ph5","dBm")) #psph5_dbm
    pp.add_int(__oid.pointCtrl(10), int(__dr.GetValue("ph5","brute"))) #psph5_Brute
    pp.add_str(__oid.pointCtrl(11), __dr.GetValue("ph6","dBm")) #psph6_dbm
    pp.add_int(__oid.pointCtrl(12), int(__dr.GetValue("ph6","brute"))) #psph6_Brute
    pp.add_str(__oid.pointCtrl(13), __dr.GetValue("ph7","dBm")) #psph7_dbm
    pp.add_int(__oid.pointCtrl(14), int(__dr.GetValue("ph7","brute"))) #psph7_Brute
    pp.add_str(__oid.pointCtrl(15), __dr.GetValue("ph8","dBm")) #psph8_dbm
    pp.add_int(__oid.pointCtrl(16), int(__dr.GetValue("ph8","brute")) )#psph8_Brute
    pp.add_str(__oid.pointCtrl(17), __dr.GetValue("ph_pompe_A","mW")) #ph_pompeA_mw
    pp.add_int(__oid.pointCtrl(18), int(__dr.GetValue("ph_pompe_A","brute")) )#ph_pompeA_Brute
    pp.add_str(__oid.pointCtrl(19), __dr.GetValue("ph_pompe_B","mW")) #ph_pompeB_mw
    pp.add_int(__oid.pointCtrl(20), int(__dr.GetValue("ph_pompe_B","brute"))) #ph_pompeB_Brute
    pp.add_str(__oid.pointCtrl(21), __dr.GetValue("temperature","")) #temperature
    pp.add_int(__oid.pointCtrl(22), int(__dr.GetValue("temperature","brute"))) #temperature_Brute
    pp.add_str(__oid.pointCtrl(23), __dr.GetValue("courant_A","")) #courantA
    pp.add_str(__oid.pointCtrl(24), __dr.GetValue("courant_A","brute")) #courantA_Brute
    pp.add_str(__oid.pointCtrl(25), __dr.GetValue("courant_B","")) #courantB
    pp.add_str(__oid.pointCtrl(26), __dr.GetValue("courant_B","brute")) #courantB_Brute
    pp.add_str(__oid.pointCtrl(27), __dr.GetdureeFonc()) #duree de fonctionement
    pp.add_str(__oid.pointCtrl(28), __dr.Getidn()) #Idn

    #Consigne
    pp.register_setter('.' + __oid.base() + '.' + __oid.consigne(1),ConsignePuissancePompeAChange)
    pp.add_str(__oid.consigne(1), str( __consignePuissancePompeAChange))
    pp.register_setter('.' + __oid.base() + '.' +__oid.consigne(2),ConsignePuissancePompeABruteChange)
    pp.add_int(__oid.consigne(2), __consignePuissancePompeABruteChange)
    pp.register_setter('.' + __oid.base() + '.' + __oid.consigne(3),ConsignePuissancePompeBChange)
    pp.add_str(__oid.consigne(3), str( __consignePuissancePompeBChange))
    pp.register_setter('.' + __oid.base() + '.' + __oid.consigne(4),ConsignePuissancePompeBBruteChange)
    pp.add_int(__oid.consigne(1), __consignePuissancePompeBBruteChange)
    
    #Commande
    pp.register_setter('.' + __oid.base() + '.' + __oid.commande(1),CommandeLaserAChange)
    pp.add_int(__oid.commande(1),int( __commandeLaserA))
    pp.register_setter('.' + __oid.base() + '.' +__oid.commande(2),CommandeLaserBChange)
    pp.add_int(__oid.commande(2),int( __commandeLaserB))

    #Traps
    pp.add_str(__oid.trap(1), __dr.GetValue("problem_temp"))
    pp.add_int(__oid.trap(3), int(__dr.GetValue("derive_signal")))
    pp.add_int(__oid.trap(5), int(__dr.GetValue("absence_signal")))
    pp.add_int(__oid.trap(7), int(__dr.GetValue("signal_trop_fort")))



def ConsignePuissancePompeAChange(oid, _type,value):
    global __consignePuissancePompeAChange
    global __dr
    try:
        __consignePuissancePompeAChange = float(value)
        pp.update()
        pp.commit()
        __dr.SetPuissance_pompe_A_mW(__consignePuissancePompeAChange)
        __dr.WriteDemande()
        return True
    except:
        return False
    
def ConsignePuissancePompeABruteChange(oid, _type,value):
    global __consignePuissancePompeABruteChange
    global __dr
    try:
        __consignePuissancePompeABruteChange = int(value)
        pp.update()
        pp.commit()
        __dr.SetPuissance_pompe_A_brute(__consignePuissancePompeABruteChange)
        __dr.WriteDemande()
        return True
    except:
        return False
    
def ConsignePuissancePompeBChange(oid, _type,value):
    global __consignePuissancePompeBChange
    global __dr
    try:
        __consignePuissancePompeBChange = float(value)
        pp.update()
        pp.commit()
        __dr.SetPuissance_pompe_B_mW(__consignePuissancePompeBChange)
        __dr.WriteDemande()
        return True
    except:
        return False
    
def ConsignePuissancePompeBBruteChange(oid, _type,value):
    global __consignePuissancePompeBBruteChange
    global __dr
    try:
        __consignePuissancePompeBBruteChange = int(value)
        pp.update()
        pp.commit()
        __dr.SetPuissance_pompe_B_brute(__consignePuissancePompeBBruteChange)
        __dr.WriteDemande()
        return True
    except:
        return False
    
def CommandeLaserAChange(oid, _type,value):
    global __commandeLaserA
    global __dr
    try:
        __commandeLaserA = int(value)
        pp.update()
        pp.commit()
        if __commandeLaserA == 0:
            __dr.SetLaserA(False)
        else:
            __dr.SetLaserA(True)
        __dr.WriteDemande()
        return True
    except:
        return False
    
def CommandeLaserBChange(oid, _type,value):
    global __commandeLaserB
    global __dr
    try:
        __commandeLaserB = int(value)
        pp.update()
        pp.commit()
        if __commandeLaserB == 0:
            __dr.SetLaserB(False)
        else:
            __dr.SetLaserB(True)
        __dr.WriteDemande()
        return True
    except:
        return False
             
        
if __name__ == '__main__':
    pp = snmp.PassPersist('.' +__oid.base())
    pp.start(update, 1)
    
                   


