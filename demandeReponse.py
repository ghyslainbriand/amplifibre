#!/usr/bin/python3
import sys
import os

class demandeReponse():
        
     def __init__(self):
         global __demandes
         global __is_windows

         __is_windows = sys.platform.startswith('win')
         __demandes = [
            ['ph1_brute'              , '?'],        #0
            ['ph1_dBm'                , '?'],        #1
            ['ph2_brute'              , '?'],        #2
            ['ph2_dBm'                , '?'],        #3
            ['ph3_brute'              , '?'],        #4
            ['ph3_dBm'                , '?'],        #5
            ['ph4_brute'              , '?'],        #6
            ['ph4_dBm'                , '?'],        #7
            ['ph5_brute'              , '?'],        #8
            ['ph5_dBm'                , '?'],        #9
            ['ph6_brute'              , '?'],        #10
            ['ph6_dBm'                , '?'],        #11
            ['ph7_brute'              , '?'],        #12
            ['ph7_dBm'                , '?'],        #13
            ['ph8_brute'              , '?'],        #14
            ['ph8_dBm'                , '?'],        #15
            ['ph_pompe_A_mW'          , '?'],        #16
            ['ph_pompe_A_brute'       , '?'],        #17
            ['ph_pompe_B_mW'          , '?'],        #18
            ['ph_pompe_B_brute'       , '?'],        #19
            ['temperature'            , '?'],        #20
            ['temperature_brute'      , '?'],        #21
            ['courant_A'              , '?'],        #22
            ['courant_A_brute'        , '?'],        #23
            ['courant_B'              , '?'],        #24
            ['courant_B_brute'        , '?'],        #25
            ['duree_fonctionnement'   , '?'],        #26
            ['idn'                    , '?'],        #27
            ['puissance_pompe_A_mW'   , '0'],        #28
            ['puissance_pompe_A_brute', '0'],        #29
            ['puissance_pompe_B_mW'   , '0'],        #30
            ['puissance_pompe_B_brute', '0'],        #31
            ['laser_A_ON'             , '0'],        #32
            ['laser_B_ON'             , '0'],        #33
            ['problem_temp'           , '0'],        #34
            ['derive_Signal'          , '0'],        #35
            ['absence_Signal'         , '0'],        #36
            ['signal_trop_fort'       , '0'],        #37
            ]
         self.__readInitDemande();

     def GetPuissance_pompe_A_mW(self):
         return (float)(__demandes[28][1])

     def GetPuissance_pompe_A_brute(self):
         return (int)(__demandes[29][1])

     def GetPuissance_pompe_B_mW(self):
         return (float)(__demandes[30][1])

     def GetPuissance_pompe_B_brute(self):
         return (int)(__demandes[31][1])

     def GetLaserA(self):
        if __demandes[32][1] == '-1':
            return True
        else:
            return False

     def GetLaserB(sefl):
        if __demandes[33][1] == '-1':
            return True
        else:
            return False
   
     def SetPuissance_pompe_A_mW(self,Value):
        __demandes[28][1] = "{}".format(Value)
              
     def SetPuissance_pompe_A_brute(self,Value):
        __demandes[29][1] = "{}".format(Value)

     def SetPuissance_pompe_B_mW(self,Value):
        __demandes[30][1] = "{}".format(Value)

     def SetPuissance_pompe_B_brute(self,Value):
        __demandes[31][1] = "{}".format(Value)

     def SetLaserA(self,Value):
         if Value:
             __demandes[32][1] = '-1'
         else:
             __demandes[32][1] = '0'
     
     def SetLaserB(self,Value):
        if Value:
             __demandes[33][1] = '-1'
        else:
             __demandes[33][1] = '0'

     def WriteDemande(self):
         if __is_windows :
              filename = "programme\Demande"
         else:
              filename = "/usr/share/Amplifibre/Demande"
         
         demandeFile = open(filename, "w") 
         for item in __demandes:
             demandeFile.write("XX\t{}\t{}\n".format(item[0],item[1]))
         demandeFile.write("\n")
         demandeFile.close()
        
     def __readInitDemande(self):
         global __demandes
         InitDemande = {}
         if __is_windows :
              filename = "programme\Demande"
         else:
              filename = "/usr/share/Amplifibre/Demande"
         if os.path.isfile(filename):
              reponseFile = open(filename, "r")
              while True:
                 line = reponseFile.readline().replace("\n","")
                 if line == '':
                     break
                 else:
                     fields = line.split("\t")
                     InitDemande[fields[1]] = fields[2]
              reponseFile.close()
              __demandes[28][1] = InitDemande[__demandes[28][0]]
              __demandes[29][1] = InitDemande[__demandes[29][0]]
              __demandes[30][1] = InitDemande[__demandes[30][0]]
              __demandes[31][1] = InitDemande[__demandes[31][0]]
              __demandes[32][1] = InitDemande[__demandes[32][0]]
              __demandes[33][1] = InitDemande[__demandes[33][0]]
          

     def readReponse(self): 
         global __reponses
         __reponses = {}
         if __is_windows :
              filename = "programme\Reponse"
         else:
              filename = "/usr/share/Amplifibre/Reponse"
              reponseFile = open(filename, "r")
         while True:
            line = reponseFile.readline().replace("\n","")
            if line == '':
                break
            elif line.startswith('#'):
                pass
            else:
                fields = line.split("\t")
                if len(fields) == 3:
                    __reponses[fields[1]] = fields[2]
         reponseFile.close()

     def GetValue(self,name,unit=""):
         if unit == "":
             key = "{}".format(name)
         else:
             key ="{}_{}".format(name, unit)
         
         if key in __reponses:
             return __reponses[key]
         else:
             return ""
     
     def GetdureeFonc(self):
         return __reponses["duree_fonctionnement"]
     
     def Getidn(self):
         return __reponses["idn"]
     
     #def __del__(self):

if __name__ == '__main__':
   dr = demandeReponse()
   
   print (dr.GetPuissance_pompe_A_mW())   
   print (dr.GetPuissance_pompe_A_brute())
   dr.SetLaserA(False)
   dr.SetLaserB(True)
   dr.SetPuissance_pompe_A_mW("111.1")
   dr.SetPuissance_pompe_A_brute("222")
   dr.SetPuissance_pompe_B_mW("333.3")
   dr.SetPuissance_pompe_B_brute("444")
   dr.readReponse()
   print (dr.GetValue("ph1","dBm"))
   print (dr.GetValue("ph2","brute"))
   print (dr.GetValue("Courant_A",""))
   print (dr.GetValue("problem_temp"))
   print (dr.GetdureeFonc())
   print (dr.Getidn())
##   
##   
##   dr.WriteDemande()
   input()
